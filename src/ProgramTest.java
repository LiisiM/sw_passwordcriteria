import org.junit.Test;

import com.google.java.contract.PreconditionError;
import com.google.java.contract.PostconditionError;

import static org.junit.Assert.assertFalse;

//@Ignore
//@Test(expected = PreconditionError.class)
//@Test(expected = PostconditionError.class)

public class ProgramTest {

    //@Ignore
    @Test(expected = PreconditionError.class)
    public void testSetMinLegth() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setMinLength(3);
    }

    //@Ignore
    @Test(expected = PreconditionError.class)
    public void testSetMaxLength() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setMaxLength(3);
    }

    @Test(expected = PreconditionError.class)
    public void testHasLettersAndNumbersFalse() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setHasLetters(false);
    }

    @Test(expected = PreconditionError.class)
    public void testLettersFalseMixedCaseTrue() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setHasNumbers(true);
        cr.setHasLetters(false);
        cr.setHasMixedCase(true);
    }

    @Test(expected = PreconditionError.class)
    public void testIsValidLengthLessThanThree() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.isValid("df");
    }

    @Test
    public void testIsValidTooLong() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setMaxLength(5);
        assertFalse(cr.isValid("adsadsadsa"));
    }

    @Test
    public void testIsValidDigitNotAllowed() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setHasNumbers(false);
        assertFalse(cr.isValid("as4f"));
    }

    @Test(expected = PostconditionError.class)
    public void testIsValidEnsuresHasMixedCase() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setHasMixedCase(true);
        cr.isValid("asdf");
    }

    @Test
    public void testIsValidMixedCaseIsTrue() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setHasMixedCase(true);
        cr.isValid("AsdF");
    }

    @Test
    public void testIsValidAllDifferent() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setHasAllDifferent(true);
        assertFalse(cr.isValid("aadf"));
    }

    @Test
    public void testIsValidAllDifferentIsTrue() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setHasAllDifferent(true);
        cr.isValid("asdf");
    }

    @Test(expected = PostconditionError.class)
    public void testIsValidEnsuresHasNumbersAndLetters() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setHasNumbers(true);
        cr.isValid("ardf");
    }

    @Test
    public void testIsValidOk() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setHasNumbers(true);
        cr.isValid("a3df");
    }

    @Test(expected = PreconditionError.class)
    public void testSetHasLettersHavingMixedCaseTrue() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setHasMixedCase(true);
        cr.setHasLetters(false);
    }

    @Test(expected = PreconditionError.class)
    public void testSetHasNumbersHavingLettersFalse() {
        PasswordCriteria cr = new PasswordCriteria();
        cr.setHasNumbers(true);
        cr.setHasLetters(false);
        cr.setHasNumbers(false);
    }
}
